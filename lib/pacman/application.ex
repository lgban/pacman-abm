defmodule Pacman.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      PacmanWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Pacman.PubSub},
      # Start the Endpoint (http/https)
      PacmanWeb.Endpoint
      # Start a worker by calling: Pacman.Worker.start_link(arg)
      # {Pacman.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Pacman.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PacmanWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
