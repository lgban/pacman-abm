defmodule Ghost1 do
  use GenServer

  def init(wallPoints) do
    {:ok, %{
      gx: 7, gy: 6,
      wallPoints: wallPoints,
      wayOut: [Up, Up, Up],
      gstate: GoingOut
    }}
  end

  def handle_call(:tick, _from, %{gx: gx, gy: gy, gstate: GoingOut, wayOut: wayOut} = state) do
    gy = gy - 1
    {
      :reply,
      %{gx: gx, gy: gy},
      %{state | gx: gx, gy: gy, gstate: (if length(wayOut) > 1, do: GoingOut, else: MovingUp), wayOut: tl(wayOut)}
    }
  end

  def handle_call(:tick, _from, %{gx: gx, gy: gy, gstate: gstate, wallPoints: wallPoints} = state) do
    opposite =
      case gstate do
        MovingRight -> "MovingLeft"
        MovingLeft -> "MovingRight"
        MovingUp -> "MovingDown"
        _ -> "MovingUp"
      end

    moves = %{
      "MovingRight" => {gx + 1, gy},
      "MovingLeft" => {gx - 1, gy},
      "MovingDown" => {gx, gy + 1},
      "MovingUp" => {gx, gy - 1}}

    {gdir, {ngx, ngy}} = moves
      |> Enum.filter(fn {_, {lgx, lgy}} ->
        lgx in 0..14 and lgy in 0..11 and ([lgx, lgy] not in wallPoints)
      end)
      |> fn l ->
        if length(l) > 1 do
          Enum.filter(l, fn {k,_} -> k != opposite end)
        else
          l
        end
      end.()
      |> Enum.random

    {
      :reply,
      %{gx: ngx, gy: ngy},
      %{state | gx: ngx, gy: ngy, gstate: String.to_atom("Elixir." <> gdir)}
    }
  end
end
