defmodule Pacman do
  use GenServer

  def init(wallPoints) do
    {:ok, %{
      x: 7, y: 10,
      wallPoints: wallPoints,
      state: MovingUp
    }}
  end

  def handle_cast({:keydown, key}, %{state: st} = state) do
    newst =
      case key do
        "ArrowRight" -> MovingRight
        "ArrowLeft" -> MovingLeft
        "ArrowDown" -> MovingDown
        "ArrowUp" -> MovingUp
        _ -> st
      end

    {:noreply, %{state | state: newst}}
  end

  def handle_cast({:keyup, key}, %{state: st} = state) do
    newst = if key in [
      "ArrowRight",
      "ArrowLeft",
      "ArrowDown",
      "ArrowUp"] do
        Immobile
      else
        st
      end
    {:noreply, %{state | state: newst}}
  end

  def handle_call(:tick, _from, %{x: x, y: y, state: st, wallPoints: wallPoints} = state) do
    {nx, ny} =
      case st do
        MovingRight when x < 14 -> {x + 1, y}
        MovingLeft when x > 0 -> {x - 1, y}
        MovingDown when y < 11 -> {x, y + 1}
        MovingUp when y > 0 -> {x, y - 1}
        _ -> {x, y}
      end

    [nx,ny] =
      if [nx,ny] in wallPoints do
        [x, y]
      else
        [nx,ny]
      end

    {:reply, %{x: nx, y: ny}, %{state | x: nx, y: ny}}
  end
end
