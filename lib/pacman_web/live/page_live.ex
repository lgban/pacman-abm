defmodule PacmanWeb.PageLive do
  use PacmanWeb, :live_view

  @maze [
    ~w[ * * * * * * * # * * * * * * * ],
    ~w[ * # * # # # * * * # * # * # * ],
    ~w[ * * * # * # # # # # * # * * * ],
    ~w[ * # # # * * * * * * * # # # * ],
    ~w[ * # * # * # # x # # * * * # * ],
    ~w[ * * * # * # x x x # * # * # * ],
    ~w[ * # * # * # x x x # * # * * * ],
    ~w[ * # * * * # # # # # * # * # * ],
    ~w[ * # # # * * * * * * * # # # * ],
    ~w[ * * * # * # # # # # * # * * * ],
    ~w[ * # * # * # * - * # # # * # * ],
    ~w[ * * * * * * * # * * * * * * * ]
  ]

  def maze, do: @maze

  @impl true
  def render(assigns) do
    # phx-window-keydown="keydown" phx-window-keyup="keyup"
    ~L"""
    <svg phx-window-keydown="keydown" phx-window-keyup="keyup" width="395" height="360" style="background:black">
      <image x="0" y="0" width="396" height="396" href="/images/maze.svg" />
      <%= Enum.map(@pellets, fn [j, i] -> %>
        <circle cx="<%= i * 24 + @xbase %>" cy="<%= j * 24 + @ybase %>" r="3" style="fill:orange" />
      <% end) %>
      <circle id="pacman-up" cx="<%= @x * 24 + @xbase %>" cy="<%= @y * 24 + @ybase %>" r="4" />
      <path d="M <%= @gx * 24 + @xbase - 6 %> <%= @gy * 24 + @ybase + 10 %> A 6 14 0 0 1 <%= @gx * 24 + @xbase + 6 %> <%= @gy * 24 + @ybase + 10 %>" style="fill:red" />
      <circle cx="<%= @gx * 24 + @xbase - 2 %>" cy="<%= @gy * 24 + @ybase + 1 %>" r="1" style="fill:white" />
      <circle cx="<%= @gx * 24 + @xbase + 2 %>" cy="<%= @gy * 24 + @ybase + 1 %>" r="1" style="fill:white" />
    </svg>
    """
  end

  def initPellets(m) do
    m
    |> Enum.with_index()
    |> Enum.flat_map(fn {row, ridx} ->
      row
      |> Enum.with_index()
      |> Enum.map(fn {c, cidx} ->
        if c == "*", do: [ridx, cidx]
      end)
      |> Enum.filter(fn pair -> pair != []  and pair != nil end)
    end)
  end

  def initWallPoints(m) do
    m
    |> Enum.with_index()
    |> Enum.flat_map(fn {row, ridx} ->
      row
      |> Enum.with_index()
      |> Enum.map(fn {c, cidx} ->
        if c == "#" or c == "x", do: [cidx, ridx]
      end)
      |> Enum.filter(fn pair -> pair != []  and pair != nil end)
    end)

  end

  @impl true
  def handle_event("keydown", %{"key" => key}, socket) do
    GenServer.cast(socket.assigns.pacman, {:keydown, key})
    {:noreply, socket}
  end

  def handle_event("keyup", %{"key" => key}, socket) do
    GenServer.cast(socket.assigns.pacman, {:keyup, key})
    {:noreply, socket}
  end

  @impl true
  def handle_info(:tick, socket) do
    Process.send_after(self(), :tick, 250) # Works better with 250
    %{x: x, y: y} = GenServer.call(socket.assigns.pacman, :tick)
    %{gx: gx, gy: gy} = GenServer.call(socket.assigns.ghost1, {:tick, x, y})

    pellets =
      if [y,x] in socket.assigns.pellets do
        IO.puts "Ate pellet at #{x}, #{y}"
        socket.assigns.pellets -- [[y, x]]
      else
        socket.assigns.pellets
      end

    {
      :noreply,
      assign(
        socket,
        x: x, y: y,
        gx: gx, gy: gy,
        pellets: pellets
      )
    }
  end

  @impl true
  def mount(_params, _session, socket) do
    Process.send_after(self(), :tick, 250)
    wallPoints = initWallPoints(maze())
    {
      :ok,
      assign(
        socket,
        xbase: 30, ybase: 66,
        x: 7, y: 10,
        gx: 6, gy: 6,
        pellets: initPellets(maze()),
        wallPoints: wallPoints,
        state: Immobile,
        pacman: GenServer.start_link(Pacman, wallPoints) |> elem(1),
        ghost1: GenServer.start_link(Ghost2, wallPoints) |> elem(1)
      )}
  end
end
