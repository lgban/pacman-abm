simulationURL = HTTPoison.post!("http://localhost:5000/games", "")
  |> Map.get(:headers)
  |> Enum.filter(fn {k, v} -> k == "Location" end)
  |> Enum.map(fn {k, v} -> v end)
  |> hd

%{"x" => gx, "y" => gy} = HTTPoison.get!(simulationURL) |> Map.get(:body) |> Jason.decode!
